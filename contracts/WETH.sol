pragma solidity ^0.7.0;

import "@openzeppelin/contracts/token/ERC20/ERC20.sol";

contract WETH is ERC20 {
    constructor() ERC20("WETH", "Wrapped Ethereum") {}

    function mint() public payable {
        _mint(msg.sender, msg.value);
    }

    function burn(uint256 _amount) public payable {
        _burn(msg.sender, _amount);
        (bool _success,  ) = msg.sender.call{value: _amount}("");
        require(_success, "fail");
    }
}