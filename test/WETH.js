const WETH = artifacts.require("WETH");
const Reverter = require("./helpers/reverter");
const truffleAssert = require("truffle-assertions");
const BN = require("bignumber.js");

contract("WETH", async(accounts) => {
    const reverter = new Reverter(web3);
    const CREATOR = accounts[0];
    const ALICE = accounts[1];
    const getBalance = web3.eth.getBalance;
    let weth;
    let WETHContract;

    before("setup", async () => {
        weth = await WETH.new({from: CREATOR});
        WETHContract = await new web3.eth.Contract(weth.abi, weth.address);
        await reverter.snapshot();
    });

    afterEach(reverter.revert);

    describe("constructor", async () => {
        it("should setup decimals", async () => {
            assert.equal(18, await weth.decimals());
        });
    });

    describe("mint, burn", async () => {
        it("should transfer ether and mint token", async () => {
            let balanceEth = await getBalance(ALICE);

            let mint = await weth.mint({from: ALICE, value: 5});

            let txMint = await web3.eth.getTransaction(mint.tx)
            let receiptMint = await web3.eth.getTransactionReceipt(mint.tx)
            let gasMint = txMint.gasPrice * receiptMint.gasUsed;
            
            assert.equal(balanceEth, parseInt(await getBalance(ALICE)) + gasMint - 5);
            assert.equal(5, await weth.balanceOf(ALICE));
        });

        it("should transfer ether and burn token", async () => {
            let balanceEth = await getBalance(ALICE);

            let mint = await weth.mint({from: ALICE, value: 5});
            let burn = await weth.burn(5, {from: ALICE});

            let txMint = await web3.eth.getTransaction(mint.tx)
            let receiptMint = await web3.eth.getTransactionReceipt(mint.tx)
            let gasMint = txMint.gasPrice * receiptMint.gasUsed;

            let txBurn = await web3.eth.getTransaction(burn.tx);
            let receiptBurn = await web3.eth.getTransactionReceipt(burn.tx)
            let gasBurn = txBurn.gasPrice * receiptBurn.gasUsed;

            assert.equal(balanceEth, parseInt(await getBalance(ALICE)) + gasMint + gasBurn);
            assert.equal(0, await weth.balanceOf(ALICE));
        });

        it("you can't burn more tokens then you have", async () => {
            truffleAssert.reverts(weth.burn(5, {from: ALICE}), "ERC20: burn amount exceeds balance");
        });
    });
});